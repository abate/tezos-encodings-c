#include "data_encodinglib.h"
#include <stdio.h>

void on_error( char* error) {
  printf("Error !!! %s\n", error);
}

int main(int argc, char **argv) {
  struct args_s *args;
  int exit_val = 0;
  FILE fp;

  if(argc==1) {
    printf("xxx\n");
    exit_val = -1;
  }
  if(argc==2) {
    printf("Encoding/Decoding from schema encoding file\n");

    char *filename = (void*)argv[1];
    FILE *f = fopen(filename, "r");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, 1, fsize, f);
    fclose(f);

    string[fsize] = 0;

    struct json* json = from_string(string, on_error);

    char* s = to_string(json);
    printf("%s\n", s);

    json_free(json);
  } else {
    printf("xxx222\n");
    exit_val = -1;
  }

  return exit_val;
}
