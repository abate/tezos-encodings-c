#include "data_encodinglib.h"
#include <stdio.h>

void on_error( char* error) {
  printf("Error !!! %s\n", error);
}

int main(int argc, char **argv) {
  struct args_s *args;
  int exit_val = 0;

  if(argc==1) {
    printf("xxx\n");
    exit_val = -1;
  }
  if(argc==2) {
    printf("Encoding/Decoding test 1\n");
    struct json* json = from_string((void*)argv[1], on_error);
    char* s = to_string(json);
    printf("%s\n", s);


    printf("\nEncoding/Decoding test 2\n");
    struct encoding* test_encoding = get_encoding("test_encoding");
//    struct json_schema* schema = json_schema((void*)test_encoding);
    void* test = destruct(test_encoding, json);
    struct json* json2 = construct(test_encoding, test);
    char* s2 = to_string(json2);
    printf("%s\n", s);

    json_free(json);
    json_free(json2);
  } else {
    printf("xxx222\n");
    exit_val = -1;
  }

  return exit_val;
}
