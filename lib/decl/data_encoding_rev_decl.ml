open Ctypes
open Foreign
(*
let parse_schemas (json: Json.t): schemas =
  Json.as_record json @@ fun get ->
  get "json_schema" |> Opt.mandatory "json_schema"
*)
(* Define a struct of callbacks (C function pointers) *)
type json = [`json] structure
type json_schema = [`json_schema] structure

let json : json typ = structure "json"
let json_schema : json_schema typ = structure "json_schema"

let json_free p =
  if (ptr_compare (to_voidp p) Ctypes.null) == 0 then ()
  else to_voidp p |> Root.release

let from_string (s: string) on_error : json ptr =
  match Data_encoding.Json.from_string s with
  |Ok j -> Root.create j |> from_voidp json
  |Error e ->
      on_error e ;
      Ctypes.null |> from_voidp json

let to_string (j: json ptr) : string =
  to_voidp j |> Root.get |> Data_encoding.Json.to_string

type 'a encoding = [`encoding] structure
let encoding : 'a encoding typ = structure "encoding"

let schema (enc: 'a encoding ptr) : json_schema ptr =
  to_voidp enc |> Root.get |> Data_encoding.Json.schema |>
  Root.create  |> from_voidp json_schema

let schema_of_json (j: json ptr) : json_schema ptr =
  to_voidp j |> Root.get |>
  Json_schema.of_json |>
  Root.create |> from_voidp json_schema

let construct (enc: 'a encoding ptr) (t: unit ptr) : json ptr =
  let enc = to_voidp enc |> Root.get in
  let t = Root.get t in
  Data_encoding.Json.construct enc t
  |> Root.create |> from_voidp json

let destruct (enc: 'a encoding ptr) (json: json ptr) : unit ptr =
  let enc = to_voidp enc |> Root.get in
  let json = to_voidp json |> Root.get in
  try Data_encoding.Json.destruct enc json |> Root.create with
        Data_encoding.Json.Cannot_destruct (_,exn) -> raise exn

type test = {v1 : int32 ; v2 : float }

let encoding_test =
  let open Data_encoding in
  conv
  (fun {v1 ; v2} -> (v1, v2))
  (fun (v1, v2) -> {v1 ; v2})
  (obj2
     (req "v1" int32)
     (req "v2" float))

let encodings = Hashtbl.create 7
let () = Hashtbl.add encodings "test_encoding" encoding_test

let get_encoding encoding_name : 'a encoding ptr =
  Hashtbl.find encodings encoding_name |> Root.create |> from_voidp encoding

let get_tezos_encoding encoding_name : 'a encoding ptr =
  let open Data_encoding.Registration in
  match find_introspectable encoding_name with
  |None -> assert false
  |Some (Any encoding) -> Root.create |> from_voidp encoding

module Rev_bindings (I : Cstubs_inverted.INTERNAL) = struct
  open Ctypes

  let () = I.structure json
  let () = I.structure json_schema

  let () = I.internal
             "from_string" (string @->
                              funptr (string @-> returning void) @->
                                returning (ptr json)) from_string

  let () = I.internal
             "to_string" (ptr json @-> returning string) to_string

  let () = I.internal
             "json_free" (ptr json @-> returning void) json_free

  let () = I.internal
             "schema_of_json" (ptr json @-> returning (ptr json_schema))
             schema_of_json

  let () = I.internal
             "get_encoding" (string @-> returning (ptr encoding)) get_encoding

  let () = I.internal
             "json_schema" (ptr encoding @->  returning (ptr json_schema))
             schema

  let () = I.internal
             "construct" (ptr encoding @->  ptr void @-> returning (ptr json))
             construct

  let () = I.internal
             "destruct" (ptr encoding @-> ptr json @-> returning (ptr void))
             destruct

end
