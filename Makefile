
all:
	dune build @install

clean:
	dune clean
	make -C tests clean

test: all
	make -C tests

build-deps:
	scripts/build-deps.sh
