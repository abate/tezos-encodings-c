#!/bin/bash

TEZOS_BRANCH=master
OPAMYES=true
PROJECT=tezos-rev-bindings

wget https://gitlab.com/tezos/tezos/raw/master/scripts/version.sh?inline=false -O scripts/version.sh

. scripts/version.sh

eval $(opam env --switch=`pwd` --set-switch)

opam switch create `pwd` ocaml-base-compiler.$ocaml_version

eval $(opam env --switch=`pwd` --set-switch)

opam install -y tezos-data-encoding dune
opam install -y ctypes ctypes-foreign

