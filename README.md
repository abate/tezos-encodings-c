# C interface for Tezos Data Encoding library

## Setup the dev environment

    make build-deps

### System dependencies

    sudo apt install libffi-dev

## Compilation

    make
    make test
